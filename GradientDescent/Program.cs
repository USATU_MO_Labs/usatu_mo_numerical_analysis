﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace GradientDescent
{
    sealed class GradientDescender : IEnumerable<GradientDescender.GradientEnumerator.StepState>
    {
        public Func<double, double, double> f;
        public Func<double, double, double> dfdx;
        public Func<double, double, double> dfdy;

        public double X0;
        public double Y0;
        public double A0;
        public double Epsilon;

        /// <summary>
        /// Создает объект для пошагового градиентного спуска по данной функции и ее производным.
        /// </summary>
        /// <param name="f">Функция от 2-х переменных.</param>
        /// <param name="dfdx">Частная производная f по x.</param>
        /// <param name="dfdy">Частная производная f по y.</param>
        /// <param name="x0">Начальное значение x.</param>
        /// <param name="y0">Начальное значение y.</param>
        /// <param name="a0">Начальная величина коэффициента шага.</param>
        /// <param name="epsilon">Точность спуска. Определяет, когда завершать спуск.</param>
        public GradientDescender(
            Func<double, double, double> f,
            Func<double, double, double> dfdx,
            Func<double, double, double> dfdy,
            double x0, double y0, double a0, double epsilon)
        {
            this.f = f;
            this.dfdx = dfdx;
            this.dfdy = dfdy;

            X0 = x0;
            Y0 = y0;
            A0 = a0;
            Epsilon = epsilon;
        }

        IEnumerator<GradientEnumerator.StepState> IEnumerable<GradientEnumerator.StepState>.GetEnumerator()
        {
            return new GradientEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (this as IEnumerable<GradientEnumerator.StepState>).GetEnumerator();
        }

        public class GradientEnumerator : IEnumerator<GradientEnumerator.StepState>
        {
            private GradientDescender Source;   // для функций и эпсилона

            private double X;
            private double Y;
            private double A;

            public GradientEnumerator(GradientDescender source)
            {
                Source = source;
                X = Source.X0;
                Y = Source.Y0;
                A = Source.A0;
            }

            StepState IEnumerator<StepState>.Current => new StepState(this);

            object IEnumerator.Current => (this as IEnumerator<StepState>).Current;

            void IDisposable.Dispose() { }

            bool IEnumerator.MoveNext()
            {
                // значение градиента в текущей точке
                var x_grad = Source.dfdx(X, Y);
                var y_grad = Source.dfdy(X, Y);

                // завершить спуск, если длина градиента в текущей точке меньше эписилона
                if (Math.Sqrt(Math.Pow(x_grad, 2) + Math.Pow(y_grad, 2)) < Source.Epsilon)
                {
                    return false;
                }

                // завершить спуск по условию 5.3 в методичке
                if (Math.Abs(x_grad) < Source.Epsilon / 2 && Math.Abs(y_grad) < Source.Epsilon / 2)
                {
                    return false;
                }

                // вычисление следующих значений X и Y
                double x_next = X, y_next = Y;
                bool first = true;
                while (first || Source.f(x_next, y_next) > Source.f(X, Y))
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        A /= 2;
                    }

                    x_next = X - A * x_grad;
                    y_next = Y - A * y_grad;
                }

                X = x_next;
                Y = y_next;
                return true;
            }

            void IEnumerator.Reset()
            {
                throw new NotSupportedException();
            }

            /// <summary>
            /// Состояние шага градиентного спуска.
            /// </summary>
            public class StepState
            {
                private readonly GradientEnumerator Source;

                public StepState(GradientEnumerator source)
                {
                    Source = source;
                }

                public double X { get { return Source.X; } }
                public double Y { get { return Source.Y; } }
                public double A { get { return Source.A; } }
                public double Epsilon { get { return Source.Source.Epsilon; } }

                public double f { get { return Source.Source.f(Source.X, Source.Y); } }
                public double dfdx { get { return Source.Source.dfdx(Source.X, Source.Y); } }
                public double dfdy { get { return Source.Source.dfdy(Source.X, Source.Y); } }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Вариант 12:
            // a = 12
            // b = -0.3
            // c = 1.21
            // d = 0.22

            var descender = new GradientDescender(
                // функция
                (x, y) => 12 * x + (-0.3 * y) + Math.Pow(Math.E, 1.21 * Math.Pow(x, 2) + 0.22 * Math.Pow(y, 2)),

                // частная производная по x
                (x, y) => 12 + Math.Pow(Math.E, 1.21 * Math.Pow(x, 2) + 0.22 * Math.Pow(y, 2)) * 2 * 1.21 * x,

                // частная производная по y
                (x, y) => -0.3 + Math.Pow(Math.E, 1.21 * Math.Pow(x, 2) + 0.22 * Math.Pow(y, 2)) * 2 * 0.22 * y,

                // x0, y0, a0, epsilon
                0, 0, 1, 10e-4);


            foreach (var state in descender)
            {
                // x y f(x, y), df/dx(x, y), df/dy(x, y)
                
                Console.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}",
                    new Object[] 
                    {
                        state.X,
                        state.Y,
                        state.f,
                        state.dfdx,
                        state.dfdy
                    }));
            }

            Console.ReadLine();
        }
    }
}