from BoundaryValueProblem import solve_diffeq_boundary
from lab_util import print_table
import math
from itertools import count


# метод конечных разностей
# y = sin(x)
# c1, c2, d1, d2 = 1
# p(x), q(x) в конце методички


if __name__ == "__main__":
    from functools import lru_cache

    @lru_cache(maxsize=65536)
    def sin(x):
        return math.sin(x)

    @lru_cache(maxsize=65536)
    def cos(x):
        return math.cos(x)

    @lru_cache(maxsize=65536)
    def sqrt(x):
        return math.sqrt(x)

    y_fun = sin
    y_deriv1 = cos
    y_deriv2 = lambda x: -sin(x)

    p = lambda x: 1 / (x**2 - 1)
    q = lambda x: 1 / sqrt(1 - x**2)

    a = 0
    b = math.pi / 4

    c1 = c2 = d1 = d2 = 1

    f = lambda x: y_deriv2(x) + p(x) * y_deriv1(x) + q(x) * y_fun(x)
    c = c1 * y_fun(a) + c2 * y_deriv1(a)
    d = d1 * y_fun(b) + d2 * y_deriv1(b)


    history = []
    for i, power in zip(range(17), count(1)):
        history.append({})
        history[i]["N"] = 2**power

        X, Y = solve_diffeq_boundary(history[i]["N"], p, q, f, a, b, c1, c2, c, d1, d2, d)

        #history[i]["delta_max"] = max(map(lambda x, y: abs(y_fun(x) - y), X, Y))
        n_mid = int(history[i]["N"] / 2)
        history[i]["y_mid"] = Y[n_mid]
        history[i]["delta_mid"] = abs(history[i]["y_mid"] - y_fun(X[n_mid]))

        print("N {}\tY_mid {}\tDelta_mid {}\t".format(history[i]["N"], history[i]["y_mid"], history[i]["delta_mid"]))

    
    #print_table(history, ["N", "delta_max"])