from typing import Callable, List, Tuple
from lab_util import create_x


def solve_diffeq_boundary(N: int, 
                          p: Callable[[float], float], q: Callable[[float], float], f: Callable[[float], float], 
                          a: float, b: float, 
                          c1: float, c2: float, c: float, 
                          d1: float, d2: float, d: float) -> Tuple[List[float], List[float]]:
    """
    Поиск приближенного решения линейного ОДУ второго порядка на отрезке [a, b] методом конечных разностей.
    ОДУ имеет вид: y'' + p(x)y' + q(x)y = f(x)
    
    Решение ОДУ должно удовлетворять следующим краевым условиям:
    c1 * y(a) + c2 * y'(a) = c
    d1 * y(b) + d2 * y'(b) = d
    abs(c1) + abs(c2) != 0
    abs(d1) + abs(d2) != 0
    
    Аргументы:
    N - количество отрезков в сетке (N + 1 точек);
    p, q, f - p(x), q(x), f(x) соответственно;
    a, b - границы отрезка, на котором происходит поиск решения;
    c1, c2, c, d1, d2, d - коэффициенты для краевых условий.

    Возвращаемое значение:
    Кортеж из двух списков - X и Y, где 
    X - N + 1 значений X, равномерно распределенных в отрезке [a, b],
    Y - N + 1 приближенных значений функции - решения данного ОДУ при соответствующих значениях X.
    """

    if abs(c1) + abs(c2) == 0:
        raise ValueError("с1 и c2 не удовлетворяют условиям")

    if abs(d1) + abs(d2) == 0:
        raise ValueError("d1 и d2 не удовлетворяют условиям")

    # шаг сетки
    h = (b - a) / N

    X = create_x(N, a, b)

    # i-е значения функций
    p_i = lambda i: p(X[i])
    q_i = lambda i: q(X[i])
    f_i = lambda i: f(X[i])

    # значения трехдиагональной матрицы
    def alpha_i(i):
        if i == 0:
            return 0    # alpha_0 (не нужно, но почему бы и нет)
        elif i == N:
            return -d2  # alpha_N
        else:
            return 1 - p_i(i) * h / 2   # alpha_i

    def beta_i(i):
        if i == 0:
            return c1 * h - c2  # beta_0
        elif i == N:
            return h * d1 + d2  # beta_N
        else:
            return -2 + q_i(i) * h**2   # beta_i

    def gamma_i(i):
        if i == 0:
            return c2   # gamma_0
        elif i == N:
            return 0    # gamma_N (пусть будет)
        else:
            return 1 + p_i(i) * h / 2   # gamma_i

    def phi_i(i):
        if i == 0:
            return h * c    # phi_0
        elif i == N:
            return h * d    # phi_N
        else:
            return  h**2 * f_i(i)   # phi_i

    # поиск прогоночных коэффициентов
    V = [-gamma_i(0) / beta_i(0)]
    U = [phi_i(0) / beta_i(0)]
    
    for i in range(1, N + 1):
        v_i = -gamma_i(i) / (beta_i(i) + alpha_i(i) * V[i - 1])
        u_i = (phi_i(i) - alpha_i(i) * U[i - 1]) / (beta_i(i) + alpha_i(i) * V[i - 1])
        
        V.append(v_i)
        U.append(u_i)

    # поиск Y обратным ходом
    Y = [0] * (N + 1)
    Y[N] = U[N]
    # i = N - 1 .. 0
    for i in range(N - 1, 0 - 1, -1):
        Y[i] = U[i] + V[i] * Y[i + 1]

    return X, Y