# Методы вычислений
## 1. Градиентный спуск
GradientDescent

## 2. Кубический сплайн
Spline

## 3. Методы численного интегрирования
NumericalIntegration

## 4. Поиск корня функции на отрезке
ZeroFunctionOnInterval (надо было назвать FunctionRootOnInterval, но уже поздно)
