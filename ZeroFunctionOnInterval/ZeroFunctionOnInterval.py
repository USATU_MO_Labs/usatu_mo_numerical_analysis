from typing import Callable, Optional


def find_function_zero(func: Callable[[float], float], a: float, b: float, eps: float = 10e-5) -> Optional[float]:
    """
    Поиск нуля функции на данном отрезке. Возвращает точку, в eps-окрестности которой находится нуль функции.
    Для функций без нуля для данного отрезка дает неверный результат.
    func - искомая функция (определенная на [a, b]);
    a, b - граници отрезка;
    eps - величиная эпсилон-окрестности (чем меньше, тем точнее).
    """
    import math

    # проверка на пересечение оси функцией f между точками a и b
    does_intersect = lambda f, a, b: math.copysign(1, f(a)) != math.copysign(1, f(b))

    # проверка на наличие пересечения (валидно только если функция монотонная)
    #if not does_intersect(func, a, b):
    #    return None

    mid = None
    while (abs(b - a) / 2 >= eps):
        mid = a + (b - a) / 2
        if does_intersect(func, mid, b):
            a = mid
        else:
            b = mid
        
    return mid
