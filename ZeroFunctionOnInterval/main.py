if __name__ == "__main__":
    from ZeroFunctionOnInterval import find_function_zero
    
    func = lambda x: x**3 + x**2 + x - 2
    #func = lambda x: 1
    a = -3
    b = 2
    eps = 10e-5

    print(find_function_zero(func, a, b, eps))
