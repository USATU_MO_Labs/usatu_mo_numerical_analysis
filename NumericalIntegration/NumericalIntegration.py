from typing import Callable
from lab_util import create_x


def simpson_integral(func: Callable[[float], float], a: float, b: float, N: int) -> float:
    """
    Вычисление приближенного значения интеграла функции методом парабол / по правилу Симпсона.
    func - функция, чей интеграл считается;
    a, b - нижний и верхний пределы интегрирования соответственно;
    N - на сколько частей разбить интервал для вычислений.
    """
    if a > b:
        raise ValueError("Invalid [a,b] interval: [{}, {}]".format(a, b))

    if N < 1:
        raise ValueError("N must be at least 1")

    h = (b - a) / N

    xs0 = create_x(N, a, b)
    ys0 = map(func, xs0[1:-1])  # для суммы y от 1 до N-1
    # взятие всех x кроме последнего для сдвига вперед на пол интервала на случай, если функция определена только на [a,b]
    xs1 = map(lambda x: x + h/2, xs0[0:-1]) # для суммы y от 0 до N-1
    ys1 = map(func, xs1)

    return h/6 * (func(a) + func(b) + 2 * sum(ys0) + 4 * sum(ys1))
