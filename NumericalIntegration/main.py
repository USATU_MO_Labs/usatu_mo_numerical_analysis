from typing import Callable, List


def test(func: Callable, a: float, b: float, int_precise_value: float = None) -> List[dict]:
    from NumericalIntegration import simpson_integral
    
    c = 0.25    # из методички по формуле M/2880 для метода парабол
    k = 4       # порядок точности метода

    history = []

    for p in range(0, 16 + 1):  # p = 0..16
        history.append({})
        N = 2**p
        history[p]["N"] = N

        int_value = simpson_integral(func, a, b, N)
        history[p]["int"] = int_value

        # K дельта
        if p >= 2:
            history[p]["K_delta"] = (history[p-2]["int"] - history[p-1]["int"]) / (history[p-1]["int"] - history[p]["int"])

        # дельта точное
        if int_precise_value != None:
            history[p]["delta_precise"] = int_precise_value - int_value

        # дельта Рунге
        if p >= 1:
            history[p]["delta_runge"] = (history[p-1]["int"] - history[p]["int"]) / (2**k - 1)

        # дельта теоретическое
        h = (b - a) / N
        history[p]["delta_theoretical"] = c * h**k

    return history


if __name__ == "__main__":
    from lab_util import print_table
    import math
    
    func = lambda x: 6 * x**5
    a = 0
    b = 1
    int_precise_value = 1
    history = test(func, a, b, int_precise_value)

    print("Отладочный тест")
    print_table(history, ["N", "K_delta", "delta_precise", "delta_runge", "delta_theoretical"])

    func = lambda x: x**(1/5) * math.sqrt(1 + x**2)
    a = 0
    b = 1.5
    history = test(func, a, b)

    print("Неберущийся интеграл 1")
    print_table(history, ["N", "K_delta", "delta_precise", "delta_runge", "delta_theoretical"])

    a = 0.001
    b = 1.5
    history = test(func, a, b)

    print("Неберущийся интеграл 2")
    print_table(history, ["N", "K_delta", "delta_precise", "delta_runge", "delta_theoretical"])