from typing import List, Tuple, Callable
import sys


def create_x(N: int, min: float, max: float) -> List[float]:
    """
    Создать список из N + 1 чисел, лежащих в отрезке [min, max]; 
    min и max - первый и последний элементы списка соответственно.
    """
    return [min + i / (N) * (max - min) for i in range(N + 1)]


def create_xy(N: int, min: float, max: float, func: Callable[[float], float]) -> Tuple[List[float], List[float]]:
    """
    Создать N + 1 точек для N отрезков из данной функции на отрезке [min, max].
    """
    xs = create_x(N, min, max)
    ys = list(map(func, xs))
    return xs, ys


def print_table(dictlist: List[dict], keys: List, sep=' ', stream=sys.stdout):
    """
    Печатает список идентичных словарей в виде таблицы в данный поток. 
    Выводятся только данные ключи в данном порядке. Столбцы называются соответственным образом.
    dictlist - список идентичных словарей;
    keys - ключи, которые следует вывести;
    sep - разделительная строка между столбцами;
    stream - поток для вывода.
    """

    # максимальные длины всех столбцов
    lengths = []
    for key in keys:
        row_lengths = list(map(lambda d: len(str(d.get(key, ""))), dictlist))
        row_lengths.append(len(key))    # длина названия столбца тоже учитывается
        length = max(row_lengths)
        lengths.append(length)

    # печать значений
    # костыль для печали заголовка с названиями столбцов
    dictlist.insert(0, keys)
    first = True
    for d in dictlist:
        for k_i in range(len(keys)):
            # {:>n.m}
            mask = "{:>" + str(lengths[k_i]) + "}"
            s = ""

            if first:   # заголовок
                s = mask.format(d[k_i])

            else:       # значения
                key = keys[k_i]
                value = d.get(key, "-")
                s = mask.format(str(value))
            
            print(s, end="", file=stream)
            
            # печать разделителя строго между столбцами
            if k_i < len(keys) - 1:
                print(sep, end="", file=stream)

        first = False
        print(file=stream)

    print(file=stream)
