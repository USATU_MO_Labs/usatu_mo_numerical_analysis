from typing import *


class Spline:
    """
    Кубический сплайн.
    """
    
    def __init__(self, xs: Iterable[float], ys: Iterable[float]):
        xs = list(xs)
        ys = list(ys)

        if len(xs) != len(ys):
            raise ValueError("Количество значений x не соответствует количеству значений y.")

        self._xs = xs
        self._ys = ys
        self.N = len(xs) - 1	# количество отрезков (индексация точек [0, N])
        
        self._ms = [None] * (self.N + 1)
        self._find_m()


    def _h(self, i: int) -> float:
        """
        i-й отрезок (i <- [1, N]).
        """
        return self._xs[i] - self._xs[i - 1]
    
    
    def _find_tridiagonal_coefficients(self) -> Tuple[List[float], List[float], List[float], List[float]]:
        """
        Поиск коэффициентов трехдиагональной матрицы.
        """
        A = [0] * (self.N + 1)
        B = [0] * (self.N + 1)
        C = [0] * (self.N + 1)
        D = [0] * (self.N + 1)

        A[0] = 1
        B[0] = 0
        C[self.N] = 0
        A[self.N] = 1

        # предпологаем, что вторые производные на концах данного отрезка равны нулю
        D[0] = D[self.N] = 0

        for i in range(1, self.N): # 1 .. N - 1
            h_i = self._h(i)
            h_ii = self._h(i + 1)

            A[i] = (h_i + h_ii) / 3
            B[i] = h_ii / 6
            C[i] = h_i / 6
            D[i] = (self._ys[i + 1] - self._ys[i]) / h_ii - (self._ys[i] - self._ys[i - 1]) / h_i

        return A, B, C, D


    def _find_m(self) -> None:
        """
        Вычисление параметров m из трехдиагональной матрицы методом прогонки.
        """
        A, B, C, D = self._find_tridiagonal_coefficients()

        # прогоночные коэффициенты lambda, mu
        la = [None] * (self.N + 1)
        mu = [None] * (self.N + 1)
        la[0] = -B[0] / A[0]
        mu[0] = D[0] / A[0]

        for i in range(1, self.N + 1):	# 1 .. N
            la[i] = -B[i] / (A[i] + C[i] * la[i - 1])
            mu[i] = (D[i] - C[i] * mu[i - 1]) / (A[i] + C[i] * la[i - 1])

        self._ms[self.N] = mu[self.N]
        for i in range(self.N - 1, -1, -1):	# N-1 .. 0
            self._ms[i] = la[i] * self._ms[i + 1] + mu[i]


    def _i_from_x(self, x: float) -> int:
        """
        Номер отрезка, в котором лежит аргумент.
        """
        for i in range(1, len(self._xs)):
            if self._xs[i - 1] <= x and x <= self._xs[i]:
                return i

        raise ValueError("Значение аргумента x не лежит ни в одном отрезке")


    def __call__(self, x: float) -> float:
        """
        Вычисление значения сплайна в точке x.
        """
        i = self._i_from_x(x)
        x_i = self._xs[i]
        x_im = self._xs[i - 1]
        y_i = self._ys[i]
        y_im = self._ys[i - 1]
        h_i = self._h(i)
        m_i = self._ms[i]
        m_im = self._ms[i - 1]

        s = ((x_i - x)**3 - h_i**2 * (x_i - x)) / (6 * h_i) * m_im \
            + ((x - x_im)**3 - h_i**2 * (x - x_im)) / (6 * h_i) * m_i \
            + (x_i - x) / h_i * y_im \
            + (x - x_im) / h_i * y_i

        return s
