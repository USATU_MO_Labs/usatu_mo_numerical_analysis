from typing import *
from Spline import Spline


def create_x(N: int, min: float, max: float) -> List[float]:
    return [min + i / (N) * (max - min) for i in range(N + 1)]


def create_xy(N: int, min: float, max: float, func: Callable) -> Tuple[List[float], List[float]]:
    """
    Создать N + 1 точек для N отрезков из данной функции на отрезке [min, max].
    """
    xs = create_x(N, min, max)
    ys = list(map(func, xs))
    return xs, ys	


def full_test() -> None:
    import math
    import itertools as it

    min_segments = 5
    p_max = 11

    N_max = min_segments * 2**p_max
    x_full = create_x(N_max, 0, math.pi)
    y_full = list(map(math.sin, x_full))
    
    # сплайны, от N_max до 5 отрезков
    splines = []
    for p in range(p_max + 1): # 0 .. p_max
        step = 2**p
        xs = it.islice(x_full, 0, None, step)
        ys = it.islice(y_full, 0, None, step)
        s = Spline(xs, ys)
        splines.append(s)

    splines.reverse()

    # вычисление погрешностей и коэффициентов для N от min_segments до N_max
    print("N\tdelta_max\tdelta_score\tk_delta")
    first = True
    delta_max_prev = None
    k_delta = None
    delta_score = None
    for p, spline in zip(it.count(0), splines):
        N = min_segments * 2**p
        
        # N значений x, при которых нужно сравнивать сплайн с функцией (очевидно, должны лежать между x, с которыми задавался сплайн)
        xs_i_iter = it.islice(it.count(0, 2**(p_max - p)), 0, N)
        i_prev = xs_i_iter.__next__()
        xs = []
        for i in xs_i_iter:
            xs.append((x_full[i] + x_full[i_prev]) / 2)

        delta_max = max(
            map(
                lambda x: math.fabs(spline(x) - math.sin(x)),
                xs
                )
            )

        if not first:
            k_delta = delta_max_prev / delta_max
            delta_score = delta_max / k_delta

        print("{:d}\t{:.2e}\t{:.2e}\t{:.2f}".format(N, delta_max, delta_score or 0, k_delta or 0))

        # подготовка к следующей итерации
        first = False;
        delta_max_prev = delta_max


def mini_test(a: float, b: float, N1: int, N2: int) -> None:
    import math
    
    spline = Spline(*create_xy(N1, a, b, math.sin))
    x2, y2 = create_xy(N2, a, b, math.sin)

    print("x\tsin\tspline\tdelta")
    max_delta = 0
    for i in range(N2):
        s = spline(x2[i])
        f = math.sin(x2[i])
        delta = math.fabs(s - f)
        max_delta = max(max_delta, delta)
        print("{:.4e}\t{:.4e}\t{:.4e}\t{:.4e}".format(x2[i], f, s, delta))

    print("Max delta: {}".format(max_delta))


def plot_test(a: float, b: float, N1: int, N2: int) -> None:
    import matplotlib.pyplot as plt
    import math

    x1, y1 = create_xy(N1, a, b, math.sin)
    spline = Spline(x1, y1)
    x, y_f = create_xy(N2, a, b, math.sin)
    y_s = list(map(spline, x))

    # график функции
    f_line, = plt.plot(x, y_f, "r")
    f_line.set_label("Функция")

    # график сплайна
    s_line, = plt.plot(x, y_s, "b")
    s_line.set_label("Сплайн")

    # точки, по которым задан сплайн
    points_line, = plt.plot(x1, y1, "go")
    points_line.set_label("Точки, по которым задан сплайн")

    plt.legend()
    plt.show()


if __name__ == "__main__":
    #plot_test(0, 10, 10, 50)
    full_test()
