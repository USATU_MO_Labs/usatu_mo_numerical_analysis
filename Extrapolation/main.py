from typing import Callable


def integral_test(f: Callable[[float], float], a: float, b: float, precise_value: float):
    """
    Тестирование экстраполяции Ричардсона на численном интегрировании методом Симпсона.
    f - интегрируемая функция;
    a, b - нижний и верхний пределы интегрирования соответственно;
    precise_value - точное значение интеграла данной функии при данных пределах интегрирования.
    """
    from Extrapolation import richardson_extrapolation
    from NumericalIntegration import simpson_integral
    from itertools import count    

    k = 4       # порядок точности
    Q = 2       # во сколько раз увеличить количество точек на каждой итерации
    
    history = []
    for i, p in zip(range(16), count(1)):
        history.append({})
        
        history[i]["n"] = Q**p
        history[i]["simpson"] = simpson_integral(f, a, b, history[i]["n"])
        history[i]["deltasimpson"] = abs(precise_value - history[i]["simpson"])

        # если 2+ итерация
        if i > 0:
            history[i]["extrapolation"] = richardson_extrapolation(
                history[i - 1]["simpson"],
                history[i]["simpson"],
                1/Q, k)

            history[i]["deltaextrapolation"] = abs(precise_value - history[i]["extrapolation"])

            history[i]["delta"] = abs(history[i]["extrapolation"] - history[i]["simpson"])

    return history


def convergent_series_test(f: Callable[[int], float], k: float, precise_sum: float):
    """
    Тестирование экстраполяции Ричардсона на суммах сходящихся рядов.
    f - функция от одного челочисленного аргумента - i-е слагаемое ряда;
    precise_sum - точное значение суммы ряда.
    """
    from Extrapolation import richardson_extrapolation
    from itertools import count

    sum_n_terms = lambda n: sum(f(i) for i in range(1, n + 1))

    Q = 2

    history = []
    for i, p in zip(range(17), count(1)):
        history.append({})

        history[i]["n"] = 2**p
        history[i]["sum"] = sum_n_terms(history[i]["n"])
        history[i]["z_n-z_precise"] = history[i]["sum"] - precise_sum;

        history[i]["extrapolation"] = richardson_extrapolation(
            history[i]["sum"],
            history[i - 1]["sum"] if i > 0 else 1,
            Q, k)

        history[i]["z_n-z_n(1)"] = history[i]["sum"] - history[i]["extrapolation"]
        history[i]["z_n(1)-z_precise"] = history[i]["extrapolation"] - precise_sum
        history[i]["delta_n"] = history[i]["z_n(1)-z_precise"] / history[i]["z_n-z_n(1)"]
           
        if i > 0:
            history[i]["second_extrapolation"] = richardson_extrapolation(
                history[i]["extrapolation"],
                history[i-1]["extrapolation"],
                Q, k + 1)
            history[i]["z_n(2)-z_precise"] = history[i]["second_extrapolation"] - precise_sum

            history[i]["delta_n(1)"] = history[i]["z_n(2)-z_precise"] / history[i]["z_n(1)-z_precise"]

    return history


if __name__ == "__main__":
    from lab_util import print_table
    import math
    
    #f = lambda x: 6 * x**5
    #a = 0
    #b = 1
    #precise_value = 1
    #history = integral_test(f, a, b, precise_value)

    #print_table(history, ["n", "simpson", "deltasimpson", "extrapolation", "deltaextrapolation", "delta"])


    f = lambda i: 1/math.pow(i, 1.1)
    k = 0.1
    precise_sum = 10.5844484649508098
    history = convergent_series_test(f, k, precise_sum)

    print_table(history, ["n", "z_n-z_precise", "z_n-z_n(1)", "z_n(1)-z_precise", "delta_n", "z_n(2)-z_precise", "delta_n(1)"])